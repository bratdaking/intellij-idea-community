-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

Format: 3.0 (quilt)
Source: intellij-idea-community
Binary: intellij-idea-community
Architecture: all
Version: 2017.2.3-1
Maintainer: Marcel Kapfer <opensource@mmk2410.org>
Standards-Version: 4.0.0
Build-Depends: debhelper (>= 7.0.50~)
Package-List:
 intellij-idea-community deb devel optional arch=all
Checksums-Sha1:
 2af60d7bb0d2765946394c4200dac6e38abe4b29 5641 intellij-idea-community_2017.2.3.orig.tar.gz
 ee9a31ab30cb3d2868a14da7a88415c1522c6357 2676 intellij-idea-community_2017.2.3-1.debian.tar.xz
Checksums-Sha256:
 93646f0ed33fb1c62473e12c18f9fa7d90ade1ec3ec2c00b92eafd9fac728882 5641 intellij-idea-community_2017.2.3.orig.tar.gz
 88fba560aa1a16d2302995de3d9bdfc10ae4e5f204930757feffdb731392205f 2676 intellij-idea-community_2017.2.3-1.debian.tar.xz
Files:
 38a2df9af92629b31beaa46222d3e00e 5641 intellij-idea-community_2017.2.3.orig.tar.gz
 ab1a5301c183c9d071f4003b5e0259f2 2676 intellij-idea-community_2017.2.3-1.debian.tar.xz

-----BEGIN PGP SIGNATURE-----

iQEcBAEBCAAGBQJZpz+6AAoJEMrebwwJ8hsJWGwH/ilWRWFx/BwwmW4bkTpzo5Hj
PbzkHBJ7sXoC+I16BFGOoYFZ9LlOvNbGxblIiaWv84S4P7aS86XG7yhOkNK/J/Ng
pMv7SXQP9D1jn1CF5OnWqJYOtFNpl/zppSn677IjNdadUJJRktWAvxyHdrxhF4Fq
AFQoIlacWLbJcVpxFd+RDNM2XXBAtBmqZ7vO8P8McpBDXL+nqkzrMeTD8Cb7OFr4
W6r0NdzJofURF0CKCKUrJ4cBiWV/UFGxxByOQtSuYuKDdTclZie6sBplWfUoRVp6
xBDVLQDq3MJe9WRhEOP6S+qJubgTapjfagQAdXptqfSgEYTz+TuCUx1yX0zlMtg=
=rnn5
-----END PGP SIGNATURE-----
